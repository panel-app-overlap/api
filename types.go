package main

type PanelsResponse struct {
	Result struct {
		PageContext struct {
			Panels []PanelVersion `json:"panels"`
		} `json:"pageContext"`
	} `json:"result"`
}

type PanelResponse struct {
	Result struct {
		PageContext PanelData `json:"pageContext"`
	} `json:"result"`
}

type PanelData struct {
	Panel struct {
		Id                int      `json:"id"`
		Name              string   `json:"name"`
		Version           string   `json:"version"`
		RelevantDisorders []string `json:"relevant_disorders"`
		SignedOff         string   `json:"signed_off"`
		Types             []struct {
			Name string `json:"name"`
		} `json:"types"`
	} `json:"panel"`
	Entities []Entity `json:"entities"`
}

type Entity struct {
	EntityName          string   `json:"entity_name"`
	EntityType          string   `json:"entity_type"`
	Tags                []string `json:"tags"`
	ModeOfPathogenicity *string  `json:"mode_of_pathogenicity,omitempty"`
	GeneData            struct {
		GeneName string   `json:"gene_name"`
		OmimGene []string `json:"omim_gene"`
	} `json:"gene_data"`
	Panel             interface{} `json:"panel"`
	ConfidenceLevel   string      `json:"confidence_level"`
	ModeOfInheritance string      `json:"mode_of_inheritance"`
	Phenotypes        []string    `json:"phenotypes"`
}

type PanelVersion struct {
	Id      int    `json:"id"`
	Version string `json:"version"`
}
