package main

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/patrickmn/go-cache"
	"strings"
)

func getGeneOverlap(ctx *fiber.Ctx) error {
	gene := ctx.Params("gene")
	gene = strings.ToUpper(gene)

	var panels []PanelData
	var err error

	cachedPanels, found := Cache.Get("panels")

	if found {
		bytes := cachedPanels.([]byte)
		json.Unmarshal(bytes, &panels)
	} else {
		panels, err = getPanels()

		if err != nil {
			return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"success": false,
				"message": "An unknown error occurred",
				"error":   1,
			})
		}

		bytes, _ := json.Marshal(panels)
		Cache.Set("panels", bytes, cache.DefaultExpiration)
	}

	matchingPanels := make([]PanelData, 0)

	for _, panel := range panels {
		for _, entity := range panel.Entities {
			if entity.EntityType == "gene" && entity.EntityName == gene {
				matchingPanels = append(matchingPanels, panel)
				break
			}
		}
	}

	for i, matchingPanel := range matchingPanels {
		if matchingPanel.Entities[0].EntityName != gene {
			var selectedEntity Entity

			for j, entity := range matchingPanel.Entities {
				if entity.EntityName == gene {
					selectedEntity = entity
					matchingPanels[i].Entities = append(matchingPanel.Entities[:j], matchingPanel.Entities[j+1:]...)
					break
				}
			}

			matchingPanels[i].Entities = append([]Entity{selectedEntity}, matchingPanels[i].Entities...)
		}
	}

	return ctx.JSON(fiber.Map{
		"success": true,
		"panels":  matchingPanels,
	})
}
