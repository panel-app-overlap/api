package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/patrickmn/go-cache"
	"log"
	"os"
	"time"

	_ "github.com/joho/godotenv/autoload"
)

var (
	Cache *cache.Cache
)

func main() {
	httpPort := os.Getenv("HTTP_PORT")

	if httpPort == "" {
		httpPort = "8080"
	}

	Cache = cache.New(24*time.Hour, 48*time.Hour)

	app := fiber.New()
	app.Use(logger.New(logger.Config{
		Format:     "${time} 🛰️ Handled request [ ${method} ${status} ${path} ]\n",
		TimeFormat: "2006/01/02 15:04:05",
	}))

	app.Use(func(ctx *fiber.Ctx) error {
		ctx.Set("access-control-allow-origin", "*")
		ctx.Set("access-control-allow-methods", "POST, GET, OPTIONS")
		ctx.Set("access-control-allow-headers", "*")

		return ctx.Next()
	})

	app.Get("/overlap/:gene", getGeneOverlap)

	log.Fatalln(app.Listen(":" + httpPort))
}
