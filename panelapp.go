package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func getPanel(version PanelVersion) (PanelData, error) {
	url := fmt.Sprintf("https://nhsgms-panelapp.genomicsengland.co.uk/page-data/panels/%d/v%s/page-data.json", version.Id, version.Version)

	res, err := http.Get(url)

	if err != nil {
		return PanelData{}, err
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return PanelData{}, err
	}

	panelResponse := PanelResponse{}

	err = json.Unmarshal(body, &panelResponse)

	if err != nil {
		return PanelData{}, err
	}

	return panelResponse.Result.PageContext, nil
}

func getPanels() ([]PanelData, error) {
	url := "https://nhsgms-panelapp.genomicsengland.co.uk/page-data/panels/page-data.json"

	res, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil, err
	}

	panelsResponse := PanelsResponse{}

	err = json.Unmarshal(body, &panelsResponse)

	if err != nil {
		return nil, err
	}

	panels := make([]PanelData, 0)

	for _, panelVersion := range panelsResponse.Result.PageContext.Panels {
		panel, err := getPanel(panelVersion)

		if err != nil {
			return nil, err
		}

		panels = append(panels, panel)
	}

	return panels, nil
}
